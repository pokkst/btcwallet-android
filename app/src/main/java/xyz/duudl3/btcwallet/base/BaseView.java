package xyz.duudl3.btcwallet.base;

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
