package xyz.duudl3.btcwallet.base;

public interface BasePresenter {
    void subscribe();
    void unsubscribe();
}
