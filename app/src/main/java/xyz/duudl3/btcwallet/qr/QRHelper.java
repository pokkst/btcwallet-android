package xyz.duudl3.btcwallet.qr;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.CaptureActivity;

public class QRHelper
{
    public QRHelper()
    {

    }

    public void startQRScan(AppCompatActivity activity) {
        if(activity.getSupportActionBar().getTitle().equals("Gallery"))
        {
            new IntentIntegrator(activity).setPrompt("Take picture").setOrientationLocked(false).setCameraId(0).setCaptureActivity(CaptureActivity.class).initiateScan();
        }
        else
        {
            new IntentIntegrator(activity).setPrompt("Scan Bitcoin Address QR").setOrientationLocked(false).setCameraId(0).setCaptureActivity(CaptureActivity.class).initiateScan();
        }
    }
}
