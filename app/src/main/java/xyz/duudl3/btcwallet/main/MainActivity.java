package xyz.duudl3.btcwallet.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Splitter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.google.zxing.qrcode.QRCodeWriter;
import com.journeyapps.barcodescanner.CaptureActivity;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.StringRes;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionConfidence;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.Wallet;
import xyz.duudl3.btcwallet.Constants;
import xyz.duudl3.btcwallet.R;
import xyz.duudl3.btcwallet.android.PermissionHelper;
import xyz.duudl3.btcwallet.crypto.HashHelper;
import xyz.duudl3.btcwallet.nfc.NFCHelper;
import xyz.duudl3.btcwallet.qr.QRHelper;
import xyz.duudl3.btcwallet.ui.NonScrollListView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends AppCompatActivity implements MainActivityContract.MainActivityView {
    public static MainActivity INSTANCE;

    private MainActivityContract.MainActivityPresenter presenter;

    @ViewById
    protected FrameLayout photosBox;
    @ViewById
    protected FrameLayout infoPage;
    @ViewById
    protected TextView infoText;
    @ViewById
    protected Button btnCloseAbout;
    @ViewById
    protected Switch offlineSwitch;
    @ViewById
    protected Switch torSwitch;
    @ViewById
    protected Button btnWriteNFC;
    @ViewById
    protected EditText txtHex;
    @ViewById
    protected FrameLayout txInfo;
    @ViewById
    protected TextView txInfoTV;
    @ViewById
    protected Button btnCloseTx;
    @ViewById
    protected Button btnViewTx;
    @ViewById
    protected FrameLayout walletSettings;
    @ViewById
    protected TextView xpub;
    @ViewById
    protected TextView seed;
    @ViewById
    protected EditText newSeed;
    @ViewById
    protected Button btnRecover;
    @ViewById
    protected Button btnClose;
    @ViewById
    protected FrameLayout passcode;
    @ViewById
    protected TextView titleLabel;
    @ViewById
    protected TextView pinDisplay;
    @ViewById
    protected Button button1;
    @ViewById
    protected Button button2;
    @ViewById
    protected Button button3;
    @ViewById
    protected Button button4;
    @ViewById
    protected Button button5;
    @ViewById
    protected Button button6;
    @ViewById
    protected Button button7;
    @ViewById
    protected Button button8;
    @ViewById
    protected Button button9;
    @ViewById
    protected Button button0;
    @ViewById
    protected Button btnCancel;
    @ViewById
    protected Button btnEnter;
    @ViewById
    protected FrameLayout flDownloadContent_LDP;
    @ViewById
    protected ProgressBar pbProgress_LDP;
    @ViewById
    protected TextView tvPercentage_LDP;
    @ViewById
    protected Toolbar toolbar_AT;
    @ViewById
    protected SwipeRefreshLayout srlContent_AM;
    @ViewById
    protected TextView tvMyBalance_AM;
    @ViewById
    protected TextView tvMyAddress_AM;
    @ViewById
    protected TextView tvRecipientAddress_AM;
    @ViewById
    protected TextView etAmount_AM;
    @ViewById
    protected TextView etFee_AM;
    @ViewById
    protected Button btnSend_AM;
    @ViewById
    protected TextView sendTitle;
    @ViewById
    protected ImageView ivCopy_AM;
    @ViewById
    protected ImageView qrCode;
    @ViewById
    protected ImageView btcLogo;
    @ViewById
    protected NonScrollListView lv_txList;
    @ViewById
    protected Button sendTab;
    @ViewById
    protected Button receiveTab;
    @ViewById
    protected Button txTab;
    @ViewById
    protected Button nfcTab;
    @ViewById
    public LinearLayout sendWindow;
    @ViewById
    public LinearLayout receiveWindow;
    @ViewById
    public LinearLayout txWindow;
    @ViewById
    public LinearLayout nfcWindow;

    @SystemService
    public ClipboardManager clipboardManager;

    @StringRes(R.string.scan_recipient_qr)
    protected String strScanRecipientQRCode;
    @StringRes(R.string.about)
    protected String strAbout;

    @ColorRes(android.R.color.holo_green_dark)
    protected int colorGreenDark;
    @ColorRes(android.R.color.darker_gray)
    protected int colorGreyDark;

    private ArrayList<Transaction> txList;
    SharedPreferences prefs = null;
    public static boolean isNewUser = true;
    private boolean offline = false;
    private boolean tor = false;

    private NFCHelper nfcHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nfcHelper = new NFCHelper(this);

        nfcHelper.readFromIntent(getIntent());
    }

    @AfterInject
    protected void initData() {
        new MainActivityPresenter(this, getCacheDir());
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcHelper.getNfcAdapter().enableForegroundDispatch(this, nfcHelper.getPendingIntent(), nfcHelper.getWriteTagFilters(), null);
    }

    @Override
    public void onPause(){
        super.onPause();
        nfcHelper.getNfcAdapter().disableForegroundDispatch(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("ApplySharedPref")
    @AfterViews
    protected void initUI() {
        INSTANCE = this;

        initToolbar();
        setListeners();
        offlineSwitch.setVisibility(View.INVISIBLE);
        torSwitch.setVisibility(View.INVISIBLE);
        prefs = getSharedPreferences("xyz.duudl3.btcwallet", MODE_PRIVATE);

        if (prefs.getBoolean("isNewUser", true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            isNewUser = true;

            prefs.edit().putBoolean("offlineMode", false).commit();
            offline = false;
            offlineSwitch.setChecked(offline);

            prefs.edit().putBoolean("usingTor", false).commit();
            tor = false;
            torSwitch.setChecked(tor);

            prefs.edit().putBoolean("isNewUser", false).commit();
        }
        else
        {
            offline = prefs.getBoolean("offlineMode", false);
            offlineSwitch.setChecked(offline);

            tor = prefs.getBoolean("usingTor", false);
            torSwitch.setChecked(tor);

            isNewUser = false;
        }

        offlineSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            offline = isChecked;
            prefs.edit().putBoolean("offlineMode", offline).commit();
        });

        torSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            tor = isChecked;
            prefs.edit().putBoolean("usingTor", tor).commit();
        });

        displaySend();
        presenter.subscribe();

        if(isNewUser) {
            titleLabel.setText("Enter New PIN");
            pinDisplay.setText("");
            passcode.setVisibility(View.VISIBLE);
            btnEnter.setOnClickListener(v -> enterPIN("new"));
        }

        PermissionHelper permissionHelper = new PermissionHelper();
        permissionHelper.askForPermissions(this, this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void displayBackupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning!");
        builder.setMessage("It is recommended to write down your recovery seed, in case " +
                "your phone is lost or stolen. You can recover this wallet with these words " +
                "on any phone with this app. Click the settings icon in the top right to see your " +
                "recovery seed.");
        builder.setCancelable(true);
        builder.setPositiveButton("Got it", (dialog, which) -> dialog.dismiss());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
        msgTxt.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OptionsItem(R.id.menuInfo_MM)
    protected void clickMenuInfo() {
        presenter.getInfoDialog();
    }

    @OptionsItem(R.id.menuRecovery_MM)
    protected void clickRecoverWindow() {
        presenter.getRecoveryWindow();
    }

    /*******************************QR SCANNER START*******************************/
    @OptionsItem(R.id.qrScan)
    protected void clickScanQR() {
        QRHelper qrHelper = new QRHelper();
        qrHelper.startQRScan(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String address = scanResult.getContents();
            if(address != null) {
                /**
                 * In Bitcoin Core at least, when generating a request payment, the QR code has the prefix "bitcoin:" and the address added onto it.
                 * We remove that here to get the raw address.
                 */
                if (address.contains("bitcoin:")) {
                    String addressFixed = address.replace("bitcoin:", "");
                    displayRecipientAddress(addressFixed);
                } else {
                    displayRecipientAddress(address);
                }
            }
        }
    }
    /*******************************QR SCANNER END*******************************/

    private void initToolbar() {
        setSupportActionBar(toolbar_AT);
        if(getSupportActionBar() != null) {
            btcLogo.setVisibility(View.GONE);
            tvMyBalance_AM.setVisibility(View.GONE);
            getSupportActionBar().setTitle("Gallery");
            //getSupportActionBar().setTitle(Constants.APP_NAME + " " + Constants.APP_VERSION);
        }
    }

    @Override
    public void setPresenter(MainActivityContract.MainActivityPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    @UiThread
    public void displayDownloadContent(boolean isShown) {
        flDownloadContent_LDP.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean isDisplayingDownload() {
        return flDownloadContent_LDP.getVisibility() == View.VISIBLE;
    }

    @Override
    @UiThread
    public void displayProgress(int percent) {
        if(pbProgress_LDP.isIndeterminate()) pbProgress_LDP.setIndeterminate(false);
        pbProgress_LDP.setProgress(percent);
    }

    @Override
    @UiThread
    public void displayPercentage(int percent) {
        tvPercentage_LDP.setText(String.valueOf(percent) + " %");

        /**to hopefully not crash, we check for the remainder when dividing by 4, and if it's 0, we update the tx list.
         good lord i hope this works, but at least it's called less often now.**/
        int remainder = percent % 4;
        if(remainder == 0) {
            setArrayAdapter(MainActivityPresenter.INSTANCE.getWallet());
        }
    }

    @Override
    @UiThread
    public void displayMyBalance(String myBalance) {
        tvMyBalance_AM.setText(myBalance);
    }

    @Override
    @UiThread
    public void displayMyAddress(String myAddress, Wallet wallet) {
        tvMyAddress_AM.setText(myAddress);
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(myAddress, BarcodeFormat.QR_CODE, 200, 200);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);

                    if(x <= 20 || x >= 180)
                    {
                        bmp.setPixel(x, y, Color.parseColor("#00353535"));
                    }

                    if(y <= 20 || y >= 180)
                    {
                        bmp.setPixel(x, y, Color.parseColor("#00353535"));
                    }
                }
            }
            ((ImageView) findViewById(R.id.qrCode)).setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        if (srlContent_AM.isRefreshing()) srlContent_AM.setRefreshing(false);

        if(!isDisplayingDownload()) {
            setArrayAdapter(wallet);
        }
    }

    @Override
    public void displayRecipientAddress(String recipientAddress) {
        if(recipientAddress != null) {
            if (TextUtils.isEmpty(recipientAddress)) {
                tvRecipientAddress_AM.setHint(strScanRecipientQRCode);
            } else {
                tvRecipientAddress_AM.setText(recipientAddress);
            }
        }
        else
        {
            tvRecipientAddress_AM.setText(null);
            tvRecipientAddress_AM.setHint(strScanRecipientQRCode);
        }
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getRecipient() {
        return tvRecipientAddress_AM.getText().toString().trim();
    }

    @Override
    public String getAmount() {
        return etAmount_AM.getText().toString();
    }

    @Override
    public String getFee() {
        return etFee_AM.getText().toString();
    }

    @Override
    public void clearAmount() { etAmount_AM.setText(null); }

    @Override
    public void clearFee() { etFee_AM.setText(null); }

    @Override
    public void displayInfoDialog() {
        if(getSupportActionBar().getTitle().equals("Gallery")) {
            infoPage.setVisibility(View.VISIBLE);
            infoText.setText("A simple photos app for Android.");
            btnCloseAbout.setOnClickListener(v -> infoPage.setVisibility(View.GONE));
        }
        else {
            infoPage.setVisibility(View.VISIBLE);
            infoText.setText("Gallery Wallet is a Bitcoin app disguised as gallery/photos app to provide privacy through obscurity.");
            btnCloseAbout.setOnClickListener(v -> infoPage.setVisibility(View.GONE));
        }
    }

    public int clicked = 0;

    @Override
    public void displayRecoveryWindow(String recoverySeed) {
        if(getSupportActionBar().getTitle().equals("Gallery")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Settings");
            builder.setMessage(Html.fromHtml("There are currently no settings to adjust."));
            builder.setCancelable(true);
            builder.setPositiveButton("Close", (dialog, which) -> dialog.dismiss());
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setMovementMethod(LinkMovementMethod.getInstance());
        }
        else {
            String xpubKey = MainActivityPresenter.INSTANCE.getXPUB();
            walletSettings.setVisibility(View.VISIBLE);
            xpub.setText(xpubKey);
            seed.setText(recoverySeed);
            newSeed.setText("");
            btnClose.setOnClickListener(v -> closeWalletInfo());
            btnRecover.setOnClickListener(v -> recoverWallet());
        }
    }
    private void closeWalletInfo()
    {
        walletSettings.setVisibility(View.GONE);
        newSeed.setText("");
    }

    private void recoverWallet()
    {
        if (newSeed.getText() != null && !TextUtils.isEmpty(newSeed.getText())) {
            /**We use this as the creation time, since it's shortly before the app was released, so no wallets of this app exist beforehand,
             * thus checking wallets for transactions would be useless.
             **/
            long creationTime = 1537401600L;
            DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(newSeed.getText()), null, "", creationTime);

            int length = Splitter.on(' ').splitToList(newSeed.getText()).size();

            if (length == 12) {
                walletSettings.setVisibility(View.GONE);
                newSeed.setText("");
                MainActivityPresenter.INSTANCE.restoreWallet(seed);
                showToastMessage("Recovery seed entered. Restoring...");
            }
        } else {
            showToastMessage("Recovery seed not entered.");
        }
    }
    @Override
    public void displayTxWindow(Transaction tx) {
        if(!isDisplayingDownload())
        {
            DecimalFormat decimalFormatter = new DecimalFormat("0.00000000");
            String receivedValueStr = MonetaryFormat.BTC.format(tx.getValue(MainActivityPresenter.INSTANCE.getWallet())).toString();
            receivedValueStr = receivedValueStr.replace("BTC ", "");
            float amtTransferred = Float.parseFloat(receivedValueStr);
            String amtTransferredStr = decimalFormatter.format(Math.abs(amtTransferred));
            String feeStr;

            if (tx.getFee() != null) {
                String feeValueStr = MonetaryFormat.BTC.format(tx.getFee()).toString();
                feeValueStr = feeValueStr.replace("BTC ", "");
                float fee = Float.parseFloat(feeValueStr);
                feeStr = decimalFormatter.format(fee);
            } else {
                feeStr = "n/a";
            }

            TransactionConfidence txConfirmations = tx.getConfidence();

            String txConfirms = "" + txConfirmations.getDepthInBlocks();
            String txDate = tx.getUpdateTime() + "";
            String txHash = tx.getHashAsString();
            txInfo.setVisibility(View.VISIBLE);
            txInfoTV.setText(Html.fromHtml("<b>BTC Transferred:</b> " + amtTransferredStr + "<br> <b>Fee:</b> " + feeStr + "<br> <b>Date:</b> " + txDate + "<br> <b>Confirmations:</b> " + txConfirms));
            btnCloseTx.setOnClickListener(v -> txInfo.setVisibility(View.GONE));
            btnViewTx.setOnClickListener(v -> {
                String url = Constants.IS_PRODUCTION ? "https://oxt.me/transaction/" : "https://testnet.blockchain.info/tx/";
                Uri uri = Uri.parse(url + txHash); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            });
        }
    }

    private void setListeners() {
        srlContent_AM.setOnRefreshListener(() -> presenter.refresh());
        btnSend_AM.setOnClickListener(v -> presenter.send());
        toolbar_AT.setOnClickListener(v -> clickImage());
        ivCopy_AM.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("My wallet address", tvMyAddress_AM.getText().toString());
            clipboardManager.setPrimaryClip(clip);
            Toast.makeText(MainActivity.this, "Copied", Toast.LENGTH_SHORT).show();
        });

        lv_txList.setOnItemClickListener((parent, view, position, id) -> {
            Transaction tx = txList.get(position);
            displayTxWindow(tx);
        });

        sendTab.setOnClickListener(v -> displaySend());
        receiveTab.setOnClickListener(v -> displayReceive());
        txTab.setOnClickListener(v -> displayTx());
        nfcTab.setOnClickListener(v -> displayNFC());

        button1.setOnClickListener(v -> addToPIN(1));
        button2.setOnClickListener(v -> addToPIN(2));
        button3.setOnClickListener(v -> addToPIN(3));
        button4.setOnClickListener(v -> addToPIN(4));
        button5.setOnClickListener(v -> addToPIN(5));
        button6.setOnClickListener(v -> addToPIN(6));
        button7.setOnClickListener(v -> addToPIN(7));
        button8.setOnClickListener(v -> addToPIN(8));
        button9.setOnClickListener(v -> addToPIN(9));
        button0.setOnClickListener(v -> addToPIN(0));
        btnCancel.setOnClickListener(v -> cancelPIN());

        btnWriteNFC.setOnClickListener(v -> {
            try {
                if (nfcHelper.getNfcTag() == null) {
                    Toast.makeText(MainActivity.this, "No NFC tag was found!", Toast.LENGTH_LONG).show();
                } else {
                    nfcHelper.write(txtHex.getText().toString(), nfcHelper.getNfcTag());
                    Toast.makeText(MainActivity.this, "Successfully wrote hex to NFC!", Toast.LENGTH_LONG ).show();
                }
            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "An error occurred.", Toast.LENGTH_LONG ).show();
                e.printStackTrace();
            } catch (FormatException e) {
                Toast.makeText(MainActivity.this, "Error formatting.", Toast.LENGTH_LONG ).show();
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        nfcHelper.readFromIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            Tag newTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            nfcHelper.setNfcTag(newTag);
        }
    }

    private void cancelPIN()
    {
        clicked = 0;
        pinDisplay.setText("");
        passcode.setVisibility(View.GONE);
    }

    private void enterPIN(String command)
    {
        if(command.equals("new"))
        {
            if(pinDisplay.getText() != null && pinDisplay.getText().length() != 0) {
                passcode.setVisibility(View.GONE);
                String pinHash = HashHelper.SHA256(pinDisplay.getText().toString());
                prefs.edit().putString("pinHash", pinHash).commit();
            }
        }
        else if(command.equals("enter")) {
            if (pinDisplay.getText() != null && pinDisplay.getText().length() != 0) {
                String pinHash = HashHelper.SHA256(pinDisplay.getText().toString());
                String pinHashStored = prefs.getString("pinHash", "");

                if (pinHash.equals(pinHashStored)) {
                    passcode.setVisibility(View.GONE);
                    btcLogo.setVisibility(View.VISIBLE);
                    tvMyBalance_AM.setVisibility(View.VISIBLE);

                    getSupportActionBar().setTitle("");

                    photosBox.setVisibility(View.GONE);

                    try {
                        if (MainActivityPresenter.INSTANCE.walletAppKit == null)
                            MainActivityPresenter.INSTANCE.setupWalletKit(null);
                        else {
                            if (MainActivityPresenter.INSTANCE.walletAppKit.isChainFileLocked()) {
                                showToastMessage("Wallet app is already loaded. Exiting this instance...");
                                System.exit(0);
                                return;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    displayDownloadContent(!isOffline());
                    offlineSwitch.setVisibility(View.VISIBLE);
                    torSwitch.setVisibility(View.VISIBLE);
                    findViewById(R.id.offlineTooltip).setVisibility(View.VISIBLE);
                    findViewById(R.id.torTooltip).setVisibility(View.VISIBLE);

                    if(isOffline())
                    {
                        btnSend_AM.setText("Get Tx Hex");
                        sendTitle.setText("Create Raw Tx");
                    }
                    else
                    {
                        btnSend_AM.setText("Send");
                        sendTitle.setText("Send");
                    }

                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    if (isNewUser)
                        displayBackupDialog();
                }
            }
        }
    }

    private void addToPIN(int num)
    {
        String pinText = pinDisplay.getText().toString();
        pinDisplay.setText(pinText + num);
    }

    private void displayTx() {
        txTab.setBackgroundColor(Color.parseColor("#f7931a"));
        sendTab.setBackgroundColor(Color.parseColor("#858585"));
        receiveTab.setBackgroundColor(Color.parseColor("#858585"));
        nfcTab.setBackgroundColor(Color.parseColor("#858585"));
        txWindow.setVisibility(View.VISIBLE);
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.GONE);
        nfcWindow.setVisibility(View.GONE);
        setArrayAdapter(presenter.getWallet());

        if(!isOffline())
            nfcTab.setVisibility(View.GONE);
    }

    private void displayReceive() {
        String myAddress = presenter.getWallet().freshReceiveAddress().toString();
        //String myAddress = SegwitAddress.fromHash(MainActivityPresenter.INSTANCE.getParameters(), presenter.getWalletKit().wallet().freshReceiveAddress().getHash()).toBech32();
        displayMyAddress(myAddress, presenter.getWallet());
        txTab.setBackgroundColor(Color.parseColor("#858585"));
        sendTab.setBackgroundColor(Color.parseColor("#858585"));
        receiveTab.setBackgroundColor(Color.parseColor("#f7931a"));
        nfcTab.setBackgroundColor(Color.parseColor("#858585"));
        txWindow.setVisibility(View.GONE);
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.VISIBLE);
        nfcWindow.setVisibility(View.GONE);

        if(!isOffline())
            nfcTab.setVisibility(View.GONE);
    }

    private void displaySend() {
        txTab.setBackgroundColor(Color.parseColor("#858585"));
        sendTab.setBackgroundColor(Color.parseColor("#f7931a"));
        receiveTab.setBackgroundColor(Color.parseColor("#858585"));
        nfcTab.setBackgroundColor(Color.parseColor("#858585"));
        txWindow.setVisibility(View.GONE);
        sendWindow.setVisibility(View.VISIBLE);
        receiveWindow.setVisibility(View.GONE);
        nfcWindow.setVisibility(View.GONE);

        if(!isOffline())
            nfcTab.setVisibility(View.GONE);
    }

    private void displayNFC() {
        txTab.setBackgroundColor(Color.parseColor("#858585"));
        sendTab.setBackgroundColor(Color.parseColor("#858585"));
        receiveTab.setBackgroundColor(Color.parseColor("#858585"));
        nfcTab.setBackgroundColor(Color.parseColor("#f7931a"));
        txWindow.setVisibility(View.GONE);
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.GONE);
        nfcWindow.setVisibility(View.VISIBLE);
    }

    public void clickImage()
    {
        if(getSupportActionBar().getTitle().equals("Gallery")) {
            clicked += 1;

            if (clicked >= 5) {
                clicked = 0;
                titleLabel.setText("Enter PIN");
                pinDisplay.setText("");
                btnEnter.setOnClickListener(v -> enterPIN("enter"));
                passcode.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void setArrayAdapter(Wallet wallet)
    {
        setListViewShit(wallet);
    }

    public void setListViewShit(Wallet wallet)
    {
        if(wallet != null) {
            List<Transaction> txListFromWallet = wallet.getRecentTransactions(100, false);

            if (txListFromWallet != null && txListFromWallet.size() != 0) {
                ArrayList<String> txListFormatted = new ArrayList<String>();
                this.txList = new ArrayList<Transaction>();

                int sizeToUse;
                if (txListFromWallet.size() >= 100)
                    sizeToUse = 100;
                else
                    sizeToUse = txListFromWallet.size();

                for (int x = 0; x < sizeToUse; x++) {
                    Transaction tx = txListFromWallet.get(x);
                    Coin value = tx.getValue(wallet);
                    DecimalFormat decimalFormatter = new DecimalFormat("0.00000000");

                    if (value.isPositive()) {
                        String receivedValueStr = MonetaryFormat.BTC.format(value).toString();
                        receivedValueStr = receivedValueStr.replace("BTC ", "");
                        float coinsTransferred = Float.parseFloat(receivedValueStr);
                        String entry = String.format("▶ %5s BTC", decimalFormatter.format(coinsTransferred));
                        txListFormatted.add(entry);
                        txList.add(tx);
                    }

                    if (value.isNegative()) {
                        String sentValueStr = MonetaryFormat.BTC.format(value).toString();
                        sentValueStr = sentValueStr.replace("BTC -", "");
                        float coinsTransferred = Float.parseFloat(sentValueStr);
                        String entry = String.format("◀ %5s BTC", decimalFormatter.format(coinsTransferred));
                        txListFormatted.add(entry);
                        txList.add(tx);
                    }

                }

                ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, txListFormatted){
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent){
                        // Get the Item from ListView
                        View view = super.getView(position, convertView, parent);

                        // Initialize a TextView for ListView each Item
                        TextView tv = (TextView) view.findViewById(android.R.id.text1);

                        // Set the text color of TextView (ListView Item)
                        tv.setTextColor(Color.WHITE);

                        // Generate ListView Item using TextView
                        return view;
                    }
                };
                lv_txList.setAdapter(itemsAdapter);

                int desiredWidth = View.MeasureSpec.makeMeasureSpec(lv_txList.getWidth(), View.MeasureSpec.UNSPECIFIED);
                int totalHeight = 0;
                View view = null;

                for (int i = 0; i < itemsAdapter.getCount(); i++) {
                    view = itemsAdapter.getView(i, view, lv_txList);

                    if (i == 0)
                        view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

                    view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += view.getMeasuredHeight();
                }

                ViewGroup.LayoutParams params = lv_txList.getLayoutParams();
                params.height = totalHeight + (lv_txList.getDividerHeight() * (itemsAdapter.getCount() - 1));

                lv_txList.setLayoutParams(params);
                lv_txList.requestLayout();
            }
        }
    }

    @Override
    public boolean isOffline()
    {
        return offline;
    }

    @Override
    public boolean usingTor()
    {
        return tor;
    }
}