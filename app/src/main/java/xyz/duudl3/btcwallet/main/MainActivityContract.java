package xyz.duudl3.btcwallet.main;

import org.bitcoinj.core.Peer;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.wallet.Wallet;

import xyz.duudl3.btcwallet.base.BasePresenter;
import xyz.duudl3.btcwallet.base.BaseView;

public interface MainActivityContract {
    interface MainActivityView extends BaseView<MainActivityPresenter> {
        void displayDownloadContent(boolean isShown);
        void displayProgress(int percent);
        void displayPercentage(int percent);

        void displayMyBalance(String myBalance);

        void displayMyAddress(String myAddress, Wallet wallet);
        void displayRecipientAddress(String recipientAddress);

        void showToastMessage(String message);
        String getRecipient();
        String getAmount();
        String getFee();
        void clearAmount();
        void clearFee();
        void displayInfoDialog();
        void displayRecoveryWindow(String seed);
        void displayTxWindow(Transaction tx);
        void setArrayAdapter(Wallet walletAppKit);
        boolean isDisplayingDownload();
        boolean isOffline();
        boolean usingTor();
    }
    interface MainActivityPresenter extends BasePresenter {
        void refresh();
        void send();
        void getInfoDialog();
        void getRecoveryWindow();
        Wallet getWallet();
    }
}
