package xyz.duudl3.btcwallet.main;

import android.content.ClipData;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.net.discovery.PeerDiscovery;
import org.bitcoinj.net.discovery.PeerDiscoveryException;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.utils.BriefLogFormatter;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.spongycastle.util.encoders.Hex;

import xyz.duudl3.btcwallet.Constants;
import xyz.duudl3.btcwallet.tx.BroadcastHelper;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.InetSocketAddress;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivityPresenter implements MainActivityContract.MainActivityPresenter {

    private MainActivityContract.MainActivityView view;
    private File walletDir;

    private NetworkParameters parameters;
    public WalletAppKit walletAppKit;
    public static MainActivityPresenter INSTANCE;
    private Wallet watchingWallet;
    private String serializedXpub;

    public MainActivityPresenter(MainActivityContract.MainActivityView view, File walletDir) {
        this.view = view;
        this.walletDir = walletDir;
        INSTANCE = this;
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        parameters = Constants.IS_PRODUCTION ? MainNetParams.get() : TestNet3Params.get();
        BriefLogFormatter.init();
    }

    public void setupWalletKit(DeterministicSeed seed)
    {
        setBitcoinSDKThread();
        walletAppKit = new WalletAppKit(parameters, walletDir, Constants.WALLET_NAME) {
            @Override
            protected void onSetupCompleted() {
                wallet().allowSpendingUnconfirmedTransactions();
                setupWalletListeners(wallet());

                if(view.isOffline())
                    refresh();
            }
        };

        if(view.isOffline()) {
            walletAppKit.setPeerNodes(null);
            walletAppKit.setDiscovery(new PeerDiscovery() {
                @Override
                public InetSocketAddress[] getPeers(long l, long l1, TimeUnit timeUnit) throws PeerDiscoveryException {
                    return null;
                }

                @Override
                public void shutdown() {

                }
            });
        }
        else {
            walletAppKit.setDownloadListener(new DownloadProgressTracker() {
                @Override
                protected void progress(double pct, int blocksSoFar, Date date) {
                    super.progress(pct, blocksSoFar, date);
                    int percentage = (int) pct;
                    view.displayPercentage(percentage);
                    view.displayProgress(percentage);
                }

                @Override
                protected void doneDownload() {
                    super.doneDownload();
                    view.displayDownloadContent(false);
                    //watchingWallet = Wallet.fromWatchingKey(parameters, DeterministicKey.deserializeB58(null, serialized, parameters));

                    refresh();
                }
            });
        }

        if(seed != null)
            walletAppKit.restoreWalletFromSeed(seed);

        walletAppKit.setBlockingStartup(false);
        walletAppKit.startAsync();
    }

    public void restoreWallet(DeterministicSeed seed)
    {
        walletAppKit.stopAsync();
        walletAppKit = null;
        view.displayDownloadContent(true);
        File walletFile = new File(walletDir, Constants.WALLET_NAME + ".wallet");
        walletFile.delete();
        File chainFile = new File(walletDir, Constants.WALLET_NAME + ".spvchain");
        chainFile.delete();

        setupWalletKit(seed);
    }

    public String getXPUB()
    {
        return serializedXpub;
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void refresh() {
        serializedXpub = walletAppKit.wallet().getWatchingKey().serializePubB58(parameters);

        view.displayMyBalance(getBalance(walletAppKit.wallet()).toFriendlyString());

        if(MainActivity.INSTANCE.txWindow.getVisibility() == View.VISIBLE) {
            view.setArrayAdapter(walletAppKit.wallet());
        }

        String myAddress = walletAppKit.wallet().freshReceiveAddress().toString();
        //String myAddress = SegwitAddress.fromHash(MainActivityPresenter.INSTANCE.getParameters(), getWalletKit().wallet().freshReceiveAddress().getHash()).toBech32();
        view.displayMyAddress(myAddress, walletAppKit.wallet());
    }

    @Override
    public void send() {
        if(!view.isDisplayingDownload()) {
            String recipientAddress = view.getRecipient();
            String amount = view.getAmount();
            String feeString = view.getFee();
            int fee;
            double amtDblToFrmt;

            if (!TextUtils.isEmpty(amount)) {
                amtDblToFrmt = Double.parseDouble(amount);
            } else {
                amtDblToFrmt = 0;
            }

            DecimalFormat df = new DecimalFormat("#.########");
            df.setRoundingMode(RoundingMode.CEILING);

            String amtDblFrmt = df.format(amtDblToFrmt);
            double amtDbl = Double.parseDouble(amtDblFrmt);

            if (!TextUtils.isEmpty(feeString)) {
                fee = Integer.parseInt(feeString);
            } else {
                fee = 0;
            }

            if (TextUtils.isEmpty(recipientAddress)) {
                view.showToastMessage("Please enter a proper recipient.");
                return;
            }
            else if (TextUtils.isEmpty(feeString) || fee <= 0) {
                view.showToastMessage("Enter a valid fee. Minimum is 1 sat/byte.");
                return;
            }
            else if (fee > 300) {
                view.showToastMessage("Enter a valid fee. Maximum fee is 300 sat/byte.");
                return;
            } else if (TextUtils.isEmpty(amount) || amtDbl <= 0.000001) {
                view.showToastMessage("Enter a valid amount. Minimum is 0.000001 BTC");
                return;
            } else if (getBalance(walletAppKit.wallet()).isLessThan(Coin.parseCoin(amtDblFrmt))) {
                view.showToastMessage("You do not have enough coins!");
                view.clearAmount();
                return;
            } else {
                if (!isValidAddress(recipientAddress)) {
                    view.showToastMessage("This is not a valid Bitcoin address.");
                    view.displayRecipientAddress(null);
                    return;
                } else {
                    Coin coinAmt = Coin.parseCoin(amtDblFrmt);

                    if (coinAmt.getValue() > 0.0) {
                        Address destination = Address.fromString(parameters, recipientAddress);
                        SendRequest req;

                        if (coinAmt.equals(getBalance(walletAppKit.wallet()))) {
                            req = SendRequest.emptyWallet(destination);
                        } else {
                            req = SendRequest.to(destination, Coin.parseCoin(amtDblFrmt));
                        }

                        req.ensureMinRequiredFee = false;
                        //converting because the UI uses sats/byte, so we need to convert that to fee per kb here
                        req.feePerKb = Coin.valueOf(Long.parseLong(fee + "") * 1000L);
                        try {
                            Transaction tx = getWallet().sendCoinsOffline(req);
                            byte[] txHexBytes = Hex.encode(tx.bitcoinSerialize());
                            String txHex = new String(txHexBytes, "UTF-8");
                            System.out.println("Created raw transaction: " + txHex);

                            /**
                             * I use the Samourai Wallet pushTx API because I've had issues sometimes where transactions sent through
                             * BitcoinJ itself to the wallet peers would not broadcast effectively, or at all. This sends a raw transaction hex to the
                             * Samourai API and then Samourai Wallet broadcasts the transaction to the network.
                             */
                            if(!view.isOffline()) {
                                System.out.println("Broadcasting raw transaction...");
                                BroadcastHelper helper = new BroadcastHelper(Constants.IS_PRODUCTION, view.usingTor());
                                helper.broadcast(txHex);
                            }
                            else
                            {
                                System.out.println("Copying raw transaction to clipboard...");
                                ClipData clip = ClipData.newPlainText("Raw Tx", txHex);
                                MainActivity.INSTANCE.clipboardManager.setPrimaryClip(clip);
                                Toast.makeText(MainActivity.INSTANCE, "Copied tx hex to clipboard", Toast.LENGTH_SHORT).show();
                                view.displayMyBalance(getBalance(getWallet()).toFriendlyString());
                                view.clearAmount();
                                view.clearFee();
                                view.displayRecipientAddress(null);
                            }
                        } catch (InsufficientMoneyException e) {
                            e.printStackTrace();
                            view.showToastMessage(e.getMessage());
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private boolean isValidAddress(String addr) {
        try {
            Address.fromString(parameters, addr);
            return true;
        } catch (AddressFormatException e) {
            return false;
        }
    }

    public Coin getBalance(Wallet wallet)
    {
        return wallet.getBalance(Wallet.BalanceType.ESTIMATED);
    }

    @Override
    public void getInfoDialog() {
        view.displayInfoDialog();
    }

    @Override
    public void getRecoveryWindow() {
        if(walletAppKit != null) {
            DeterministicSeed seed = walletAppKit.wallet().getKeyChainSeed();
            final List<String> mnemonicCode = seed.getMnemonicCode();
            String recoverySeed = "";

            for (int x = 0; x < mnemonicCode.size(); x++) {
                recoverySeed += mnemonicCode.get(x) + " ";
            }

            view.displayRecoveryWindow(recoverySeed);
        }
        else
        {
            view.displayRecoveryWindow("");
        }
    }

    private void setBitcoinSDKThread() {
        final Handler handler = new Handler();
        Threading.USER_THREAD = handler::post;
    }

    private void setupWalletListeners(Wallet wallet) {
        wallet.addCoinsReceivedEventListener((wallet1, tx, prevBalance, newBalance) -> {
            if(!view.isDisplayingDownload()) {
                view.displayMyBalance(getBalance(wallet1).toFriendlyString());

                if (tx.getPurpose() == Transaction.Purpose.UNKNOWN)
                    view.showToastMessage("Received " + newBalance.minus(prevBalance).toFriendlyString());

                refresh();

                view.setArrayAdapter(wallet1);
            }
        });
        wallet.addCoinsSentEventListener((wallet12, tx, prevBalance, newBalance) -> {
            if(!view.isDisplayingDownload() && !view.isOffline()) {
                view.displayMyBalance(getBalance(wallet12).toFriendlyString());
                view.clearAmount();
                view.clearFee();
                view.displayRecipientAddress(null);
                view.showToastMessage("Sent " + prevBalance.minus(newBalance).minus(tx.getFee()).toFriendlyString());
                refresh();

                view.setArrayAdapter(wallet12);
            }
        });
    }

    public Wallet getWallet()
    {
        return walletAppKit.wallet();
    }
}
